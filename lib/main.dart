import 'package:bloc_pattern/bloc_pattern.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:help_maintenance/features/user/user_bloc.dart';
import 'package:help_maintenance/features/video_assistent/blocs/favorites_bloc.dart';
import 'package:help_maintenance/features/video_assistent/video_screen.dart';
import 'features/home/home.dart';
import 'features/login/login_screen.dart';
import 'features/video_assistent/blocs/videos_bloc.dart';


void main() {
  runApp(App());

}

class App extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return BlocProvider(
      bloc: UserBloc(),
      child: BlocProvider(
        bloc: VideosBloc(),
        child: BlocProvider(
          bloc: FavoriteBloc(),
          child: MaterialApp(
            debugShowCheckedModeBanner: false,
            theme: ThemeData(fontFamily: "Roboto-Medium"),
            title: "Help Maintenance APP",
            initialRoute: LoginScreen.id,
            routes:{
              LoginScreen.id: (context) => LoginScreen(),
              HomeScreen.id: (context) => HomeScreen(),
            },
          ),
        ),
      ),
    );
  }
}
