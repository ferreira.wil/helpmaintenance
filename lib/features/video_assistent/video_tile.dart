import 'package:bloc_pattern/bloc_pattern.dart';
import 'package:flutter/material.dart';
import 'package:flutter_youtube/flutter_youtube.dart';
import 'package:help_maintenance/features/video_assistent/api.dart';
import 'package:help_maintenance/features/video_assistent/blocs/favorites_bloc.dart';
import 'package:help_maintenance/models/video.dart';

class VideoTile extends StatelessWidget {
  final Video video;

  VideoTile(this.video);

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: () {
        FlutterYoutube.playYoutubeVideoById(
            apiKey: API_KEY,
            videoId: video.id);
      },
      child: Container(
        margin: EdgeInsets.symmetric(vertical: 4),
        child: Card(
          color: Color(0xFFE8E8E8),
          elevation: 10,
          child: InkWell(
            splashColor: Colors.blue.withAlpha(30),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.stretch,
              children: <Widget>[
                ClipRRect(
                  borderRadius: BorderRadius.circular(15),
                  child: AspectRatio(
                    aspectRatio: 16.0 / 9.0,
                    child: Image.network(video.thumb, fit: BoxFit.cover),
                  ),
                ),
                Row(
                  children: <Widget>[
                    Expanded(
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: <Widget>[
                          Padding(
                            padding: EdgeInsets.fromLTRB(8, 8, 8, 0),
                            child: Text(
                              video.title,
                              maxLines: 2,
                              style: TextStyle(fontSize: 16, fontWeight: FontWeight.bold,color: Color(0xFF5c88da)),
                            ),
                          ),
                        ],
                      ),
                    ),
                    StreamBuilder <Map<String,Video>>(
                      stream: BlocProvider.of<FavoriteBloc>(context).outFav,
                      initialData: {},
                      builder: (context,snapshot){
                        if(snapshot.hasData) {
                          return IconButton(
                            icon: Icon(snapshot.data.containsKey(video.id) ?
                            Icons.star :
                            Icons.star_border),
                            color: snapshot.data.containsKey(video.id)? Colors.amberAccent: Colors.grey,
                            onPressed: (){
                              BlocProvider.of<FavoriteBloc>(context).toggleFavorite(video);
                            },
                          );
                        }
                        else{
                          return CircularProgressIndicator();
                        }
                      },
                    )
                  ],
                )
              ],
            ),
          ),
        ),
      ),
    );
  }
}


