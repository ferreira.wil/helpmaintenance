import 'package:bloc_pattern/bloc_pattern.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:help_maintenance/features/video_assistent/blocs/video_player_controller.dart';
import 'package:help_maintenance/features/video_assistent/favorites_screen.dart';
import 'package:help_maintenance/features/video_assistent/video_google_search_screen.dart';
import 'package:help_maintenance/features/video_assistent/video_tile_google.dart';
import 'blocs/videos_bloc.dart';
import 'blocs/favorites_google_blocs.dart';


class VideoPlayerScreen extends StatelessWidget {
  final videoPlayer = VideoPlayerBlocs();
  final favoritesBlocs = FavoriteGoogleBloc();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        backgroundColor: Colors.white,
        appBar: AppBar(
          backgroundColor: Color(0xFF1f345f),
          actions: <Widget>[
            Align(
              alignment: Alignment.center,
              child: StreamBuilder(
                  stream: favoritesBlocs.outGoogleFav,
                  initialData: {},
                  builder: (context, snapshot) {
                    if(snapshot.hasData) {
                      return Text("${snapshot.data.length}");
                    }else{
                      return Container();}
                  }),
            ),
            IconButton(
              icon: Icon(Icons.star),
              onPressed: () {
                Navigator.push(context, MaterialPageRoute(builder: (context) => FavoritesScreen()));
              },
            ),
          ],
        ),
        body: Column(
          children: <Widget>[
            SearchBox(
              onSubmitted: (value) {
                videoPlayer.searchVideo(value);
                Navigator.push(context, MaterialPageRoute(builder: (context) => VideoGooglesSearchScreen(videoSearchController: videoPlayer,favoritesSearchBlocs: favoritesBlocs,)));

              },
            ),
            SingleChildScrollView(
              scrollDirection: Axis.horizontal,
              child: StreamBuilder(
                stream: videoPlayer.outCategory,
                initialData: Category.SMP,
                builder: (context, snapshot) {
                  if(snapshot.hasData){
                    switch(snapshot.data){
                      case Category.SMP:
                        return Row(
                          children: <Widget>[
                            CategoryItem(
                              title: "SMP",
                              isActive: true,
                              press: (){
                                videoPlayer.choseCategory(Category.SMP);
                              },
                            ),
                            CategoryItem(
                              title: "Limpeza",
                              isActive: false,
                              press: (){
                                videoPlayer.choseCategory(Category.CLEANING);
                              },
                            ),
                            CategoryItem(
                              title: "Substituição",
                              isActive: false,
                              press: (){
                                videoPlayer.choseCategory(Category.SUBS);
                              },
                            ),
                            CategoryItem(
                              title: "Inspeção",
                              isActive: false,
                              press:(){
                                videoPlayer.choseCategory(Category.INSPECTING);
                              },
                            )
                          ],
                        );
                        break;
                      case Category.CLEANING:
                        return Row(
                          children: <Widget>[
                            CategoryItem(
                              title: "SMP",
                              isActive: false,
                              press: (){
                                videoPlayer.choseCategory(Category.SMP);
                              },
                            ),
                            CategoryItem(
                              title: "Limpeza",
                              isActive: true,
                              press: (){
                                videoPlayer.choseCategory(Category.CLEANING);
                              },
                            ),
                            CategoryItem(
                              title: "Substituição",
                              isActive: false,
                              press: (){
                                videoPlayer.choseCategory(Category.SUBS);
                              },
                            ),
                            CategoryItem(
                              title: "Inspeção",
                              isActive: false,
                              press:(){
                                videoPlayer.choseCategory(Category.INSPECTING);
                              },
                            )
                          ],
                        );
                        break;
                      case Category.SUBS:
                        return Row(
                          children: <Widget>[
                            CategoryItem(
                              title: "SMP",
                              isActive: false,
                              press: (){
                                videoPlayer.choseCategory(Category.SMP);
                              },
                            ),
                            CategoryItem(
                              title: "Limpeza",
                              isActive: false,
                              press: (){
                                videoPlayer.choseCategory(Category.CLEANING);
                              },
                            ),
                            CategoryItem(
                              title: "Substituição",
                              isActive: true,
                              press: (){
                                videoPlayer.choseCategory(Category.SUBS);
                              },
                            ),
                            CategoryItem(
                              title: "Inspeção",
                              isActive: false,
                              press:(){
                                videoPlayer.choseCategory(Category.INSPECTING);
                              },
                            )
                          ],
                        );
                        break;
                      case Category.INSPECTING:
                        return Row(
                          children: <Widget>[
                            CategoryItem(
                              title: "SMP",
                              isActive: false,
                              press: (){
                                videoPlayer.choseCategory(Category.SMP);
                              },
                            ),
                            CategoryItem(
                              title: "Limpeza",
                              isActive: false,
                              press: (){
                                videoPlayer.choseCategory(Category.CLEANING);
                              },
                            ),
                            CategoryItem(
                              title: "Substituição",
                              isActive: false,
                              press: (){
                                videoPlayer.choseCategory(Category.SUBS);
                              },
                            ),
                            CategoryItem(
                              title: "Inspeção",
                              isActive: true,
                              press:(){
                                videoPlayer.choseCategory(Category.INSPECTING);
                              },
                            )
                          ],
                        );
                        break;
                      default:
                        return CircularProgressIndicator();

                    }
                  }else{
                    return CircularProgressIndicator();
                  }

                }
              ),
            ),
            ListVideos(videoPlayer: videoPlayer.outGoogleVideos,favoritesGoogle: favoritesBlocs,),
          ],
        )
    );
  }
}

class CategoryItem extends StatelessWidget {
  final String title;
  final bool isActive;
  final Function press;

  const CategoryItem({Key key, this.title, this.isActive = false, this.press})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: press,
      child: Padding(
        padding: EdgeInsets.symmetric(horizontal: 20, vertical: 15),
        child: Column(
          children: <Widget>[
            Text(
              title,
              style: isActive ? TextStyle(
                  color: Color(0xFF5c88da),
                  fontWeight: FontWeight.bold,
                  fontSize: 20) : TextStyle(
                  color: Color(0xFFB5BFD0),
                  fontWeight: FontWeight.bold,
                  fontSize: 18)

            ),
            Container(
              margin: EdgeInsets.symmetric(vertical: 5),
              height: 3,
              width: 22,
              decoration: isActive ? BoxDecoration(
                  color: Color(0xFF50505D),
                  borderRadius: BorderRadius.circular(10)): BoxDecoration(
                  color: Colors.white,
                  borderRadius: BorderRadius.circular(10))
            ),
          ],
        ),
      ),
    );
  }
}

class SearchBox extends StatelessWidget {
  final ValueChanged<String> onSubmitted;

  const SearchBox({Key key, this.onSubmitted}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      margin: EdgeInsets.all(10),
      padding: EdgeInsets.symmetric(horizontal: 20, vertical: 1),
      decoration: BoxDecoration(
          borderRadius: BorderRadius.circular(30),
          border: Border.all(color: Color(0xFFB5BFD0).withOpacity(0.32))),
      child: TextField(
        onSubmitted: onSubmitted,
        decoration: InputDecoration(
          border: InputBorder.none,
          icon: Icon(Icons.search),
          hintText: "Procure por videos aqui... ",
          hintStyle: TextStyle(color: Color(0xFFB5BFD0)),
        ),
      ),
    );
  }
}

class ListVideos extends StatelessWidget {
  final videoPlayer;
  final favoritesGoogle;

  ListVideos({Key key, this.videoPlayer, this.favoritesGoogle}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Expanded(
      child: StreamBuilder(
        stream: videoPlayer,
        builder: (context, snapshot) {
          if (snapshot.hasData) {
            return ListView.builder(
              itemBuilder: (context, index) {
                if (index < snapshot.data.length) {
                  return VideoTileGoogle(snapshot.data[index],favoritesGoogle);
                } else {
                  return Container(
                    height: 40,
                    width: 40,
                    alignment: Alignment.center,
                    child: CircularProgressIndicator(
                      valueColor: AlwaysStoppedAnimation<Color>(Colors.lightBlue),
                    ),
                  );
                }
              },
              itemCount: snapshot.data.length,
            );
          } else {
            return Container();
          }
        },
      ),
    );
  }
}
