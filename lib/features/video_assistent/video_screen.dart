import 'package:bloc_pattern/bloc_pattern.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:help_maintenance/features/video_assistent/video_search.dart';
import 'package:help_maintenance/features/video_assistent/video_tile.dart';

import 'blocs/favorites_bloc.dart';
import 'blocs/videos_bloc.dart';

class VideoScreen extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,
      appBar: AppBar(
        backgroundColor: Color(0xFF1f345f),
        actions: <Widget>[
          Align(
            alignment: Alignment.center,
            child: StreamBuilder(
                stream: BlocProvider.of<FavoriteBloc>(context).outFav,
                initialData: {},
                builder: (context, snapshot) {
                  if(snapshot.hasData) {
                    return Text("${snapshot.data.length}");
                  }else{
                    return Container();}
            }),
          ),
          IconButton(
            icon: Icon(Icons.star),
            onPressed: () {},
          ),
          Padding(
            padding: EdgeInsets.fromLTRB(0, 5, 0, 0),
            child: IconButton(
              icon: Icon(
                Icons.search,
                size: 30,
              ),
              onPressed: () async {
                String result =
                    await showSearch(context: context, delegate: VideoSearch());
                if (result != null)
                  BlocProvider.of<VideosBloc>(context).inSearch.add(result);
              },
            ),
          ),
        ],
      ),
      body: StreamBuilder(
        stream: BlocProvider.of<VideosBloc>(context).outVideos,
        builder: (context, snapshot) {
          if (snapshot.hasData) {
            return ListView.builder(
              itemBuilder: (context, index) {
                if (index < snapshot.data.length) {
                  return VideoTile(snapshot.data[index]);
                } else {
                  BlocProvider.of<VideosBloc>(context).inSearch.add(null);
                  return Container(
                    height: 40,
                    width: 40,
                    alignment: Alignment.center,
                    child: CircularProgressIndicator(
                      valueColor:
                          AlwaysStoppedAnimation<Color>(Colors.lightBlue),
                    ),
                  );
                }
              },
              itemCount: snapshot.data.length + 1,
            );
          } else {
            return StreamBuilder(
              stream: BlocProvider.of<FavoriteBloc>(context).outFav,
              builder: (context,snapshot){
                if(snapshot.hasData){
                  if(snapshot.data.length != 0){
                  return ListView.builder(
                      itemCount: snapshot.data.length,
                      itemBuilder: (context, index){
                        return VideoTile(snapshot.data.values.toList()[index]);
                      });
                  }else{
                    return Container(
                      margin: EdgeInsets.all(35),
                      child: Padding(
                        padding: EdgeInsets.all(5.0),
                        child: Column(
                          mainAxisAlignment: MainAxisAlignment.start,
                          crossAxisAlignment: CrossAxisAlignment.center,
                          children: <Widget>[
                            Image(image: AssetImage("images/estrelatriste.png"),),
                            Padding(
                              padding: EdgeInsets.only(top: 10.0),
                              child: Text("Você ainda não possui vídeos favoritos",textAlign: TextAlign.center,style: TextStyle(fontWeight: FontWeight.bold,fontSize: 20.0),),
                            )
                          ],
                        ),
                      ),
                    );
                  }
                }
                else
                  return Container(
                    margin: EdgeInsets.all(35),
                    child: Padding(
                      padding: EdgeInsets.all(5.0),
                      child: Column(
                        mainAxisAlignment: MainAxisAlignment.start,
                        crossAxisAlignment: CrossAxisAlignment.center,
                        children: <Widget>[
                          Image(image: AssetImage("images/estrelatriste.png"),),
                          Padding(
                            padding: EdgeInsets.only(top: 10.0),
                            child: Text("Você ainda não possui vídeos favoritos",textAlign: TextAlign.center,style: TextStyle(fontWeight: FontWeight.bold,fontSize: 20.0),),
                          )
                        ],
                      ),
                    ),
                  );
              },
            );
          }
        },
      ),
    );
  }
}
