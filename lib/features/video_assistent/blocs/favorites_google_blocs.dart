import 'dart:async';
import 'dart:convert';
import 'package:bloc_pattern/bloc_pattern.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:rxdart/rxdart.dart';
import 'package:shared_preferences/shared_preferences.dart';

class FavoriteGoogleBloc implements BlocBase{

  Map<String, Map<String,String>> _favoritesGoogle ={};

  final StreamController<Map<String, Map<String,String>>> _favGoogleController = BehaviorSubject<Map<String, Map<String,String>>>();
  Stream<Map<String, Map<String,String>>> get outGoogleFav => _favGoogleController.stream;


  FavoriteGoogleBloc(){
    SharedPreferences.getInstance().then((prefs){
      if(prefs.getKeys().contains("favorites")){



         _favoritesGoogle = fromJson(json.decode(prefs.getString("favorites")));
        _favGoogleController.sink.add(_favoritesGoogle);

        //prefs.remove("favorites");
      }

    });
  }


  Map<String, Map<String,String>> fromJson(Map<String,dynamic> json){
    Map<String, Map<String,String>> mapGoogleFav = {};

    json.forEach((key, value) {
      mapGoogleFav.addAll({
        key:{

          "videoID": value["videoID"],
          "category": value["category"]
        }
      });
    });

    return mapGoogleFav;
  }

  void toggleGoogleFavorites(DocumentSnapshot video){
    if(_favoritesGoogle.containsKey(video.data["id"])){

      _favoritesGoogle.remove(video.data["id"]);
    }
    else{

      _favoritesGoogle[video.data["id"]] = {
        "videoID": video.data["id"],
        "category": video.data["categoria"]
      };

    }

    _favGoogleController.sink.add(_favoritesGoogle);

    _saveGoogleFav();
  }

  void _saveGoogleFav(){
    SharedPreferences.getInstance().then((prefs){
      prefs.setString("favorites", json.encode(_favoritesGoogle));
    });

  }




  @override
  void dispose() {

    _favGoogleController.close();
  }

}