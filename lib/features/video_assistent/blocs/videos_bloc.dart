import 'dart:async';
import 'package:bloc_pattern/bloc_pattern.dart';
import 'package:help_maintenance/features/video_assistent/api.dart';
import 'package:help_maintenance/models/video.dart';
import 'package:rxdart/rxdart.dart';

class VideosBloc implements BlocBase {
  Api api;

  List<Video> videos;

  final StreamController<List<Video>> _videosController = BehaviorSubject<List<Video>>();
  final StreamController<String> _searchController = BehaviorSubject<String>();


  Stream get outVideos => _videosController.stream;
  Sink get inSearch => _searchController.sink;

  VideosBloc() {
    api = Api();

    _searchController.stream.listen(_search);
  }

  void _search(String search) async {
    if (search != null) {
      videos = await api.search(search);
    } else {
      videos += await api.nextPage();
    }

    _videosController.sink.add(videos);
  }

  @override
  void dispose() {
    _videosController.close();
    _searchController.close();
  }
}
