import 'dart:async';

import 'package:bloc_pattern/bloc_pattern.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_storage/firebase_storage.dart';
import 'package:rxdart/rxdart.dart';

enum Category {SMP,CLEANING,SUBS,INSPECTING}

class VideoPlayerBlocs implements BlocBase{

  StreamController<List<DocumentSnapshot>> _videoPlayerController = BehaviorSubject<List<DocumentSnapshot>>();
  StreamController<Category> _categoryController = BehaviorSubject<Category>();
  StreamController<List<DocumentSnapshot>> _specifcVideosController = BehaviorSubject<List<DocumentSnapshot>>();
  StreamController<List<DocumentSnapshot>> _videoSearchController = BehaviorSubject<List<DocumentSnapshot>>();


  Stream get outGoogleVideos => _videoPlayerController.stream;
  Stream get outCategory => _categoryController.stream;
  Stream get outSpecifcVideos => _specifcVideosController.stream;
  Stream get outSearchVideos => _videoSearchController.stream;

  VideoPlayerBlocs(){
    getVideos(Category.SMP);
  }

  choseCategory(Category chosed) async{
    _categoryController.sink.add(chosed);
    await getVideos(chosed);
  }

  getVideos(Category videoType) async {
    Stream<QuerySnapshot> videos;

    switch(videoType){
      case Category.SMP:
        videos = await Firestore.instance.collection("videos").document("smp").collection("smp").snapshots();
        break;

      case Category.SUBS:
        videos = await Firestore.instance.collection("videos").document("substituicao").collection("substituicao").snapshots();
      break;

      case Category.CLEANING:
          videos = await Firestore.instance.collection("videos").document("limpeza").collection("limpeza").snapshots();
          break;
      case Category.INSPECTING:
        videos = await Firestore.instance.collection("videos").document("inspecao").collection("inspecao").snapshots();

        break;
      default:
        videos = await Firestore.instance.collection("videos").document("smp").collection("smp").snapshots();
        break;



    }

    videos.listen((event) {

      _videoPlayerController.sink.add(event.documents);
    });
  }
  
  getSpecificVideo(List<Map<String,String>> favorites) async {
    Stream<QuerySnapshot> videos;
    List<DocumentSnapshot> specifcVideos= [];

    favorites.forEach((element) async {
      videos = await Firestore.instance.collection("videos").document(element["category"]).collection(element["category"]).where("id",isEqualTo: element["videoID"]).snapshots();

      videos.listen((event) {
        print("Videos.listen: ${event.documents}");
        specifcVideos.add(event.documents[0]);
        _specifcVideosController.sink.add(specifcVideos);

      });

    });

  }

  searchVideo(String title) async {
    Stream<QuerySnapshot> videos;
    List<DocumentSnapshot> specifcVideos= [];

    title = title.toLowerCase();

    print(title);

    videos = await Firestore.instance.collection("videos").document("allVideos").collection("allVideos").snapshots();

    videos.listen((event) {

      event.documents.forEach((element) {
        if((element.data["title"].toString().toLowerCase()).contains(title)){
          specifcVideos.add(element);
          _videoSearchController.sink.add(specifcVideos);
        }

      });

    });
    
  }


  @override
  void dispose() {
    // TODO: implement dispose
    _videoPlayerController.close();
    _categoryController.close();
    _specifcVideosController.close();
    _videoSearchController.close();
  }

}


