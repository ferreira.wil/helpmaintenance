import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:help_maintenance/features/video_assistent/blocs/favorites_google_blocs.dart';
import 'package:help_maintenance/features/video_assistent/blocs/video_player_controller.dart';
import 'package:help_maintenance/features/video_assistent/video_player_screen.dart';
import 'package:url_launcher/url_launcher.dart';

class FavoritesScreen extends StatelessWidget {
  final googleFav = FavoriteGoogleBloc();
  final _toGetSpecificVideos = VideoPlayerBlocs();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        backgroundColor: Colors.white,
        appBar: AppBar(
        backgroundColor: Color(0xFF1f345f),
          title: Center(child: Text("Favoritos")),
        ),
        body: StreamBuilder<Map<String, Map<String,String>>>(
          stream: googleFav.outGoogleFav,
          builder: (context, snapshot) {
            if(snapshot.hasData){
              print(snapshot.data.values.toList());
              _toGetSpecificVideos.getSpecificVideo(snapshot.data.values.toList());
              return StreamBuilder(
                stream: _toGetSpecificVideos.outSpecifcVideos,
                builder: (context,snapshot) {
                  if(snapshot.hasData){
                    print(snapshot.data);
                    return ListView.builder(
                      itemCount: snapshot.data.length,
                      itemBuilder: (context,int index){
                        return Padding(
                          padding: const EdgeInsets.all(8.0),
                          child: GestureDetector(
                            onTap: ()async {
                              print(snapshot.data[index].data["url"]);
                              var downloadUrl =
                              snapshot.data[index].data["url"];
                              if (await canLaunch(downloadUrl)) {
                                await launch(downloadUrl);
                              } else {
                                throw 'Could not launch $downloadUrl';
                              }
                            },
                            child: InkWell(
                              splashColor: Colors.blueGrey,
                              child: Card(
                                elevation: 15,
                                child: Row(
                                  children: <Widget>[
                                    Padding(
                                      padding: EdgeInsets.only(right: 5),
                                      child: ClipRRect(
                                        borderRadius: BorderRadius.circular(15),
                                        child: Container(
                                            width: 110,
                                            height: 60,
                                            child: Image.memory(base64.decode(snapshot.data[index].data["thumb"])))
                                      ),

                                    ),
                                    Expanded(
                                        child: Center(child: Text(snapshot.data[index].data["title"],style: TextStyle(fontSize: 16, fontWeight: FontWeight.bold,color: Color(0xFF5c88da)),))),

                                  ],
                                ),
                              ),
                            ),
                          ),
                        );
                      },

                    );
                  }else{
                    return Container(
                      margin: EdgeInsets.all(35),
                      child: Padding(
                        padding: EdgeInsets.all(5.0),
                        child: Column(
                          mainAxisAlignment: MainAxisAlignment.start,
                          crossAxisAlignment: CrossAxisAlignment.center,
                          children: <Widget>[
                            Image(image: AssetImage("images/estrelatriste.png"),),
                            Padding(
                              padding: EdgeInsets.only(top: 10.0),
                              child: Text("Você ainda não possui vídeos favoritos",textAlign: TextAlign.center,style: TextStyle(fontWeight: FontWeight.bold,fontSize: 20.0),),
                            )
                          ],
                        ),
                      ),
                    );
                  }
                }
              );

            }else{
              return Center(child: CircularProgressIndicator());
            }
          }
        ),
    );
  }
}

/*                ClipRRect(
                  borderRadius: BorderRadius.circular(15),
                  child: AspectRatio(
                    aspectRatio: 16.0 / 9.0,
                    //MemoryImage(base64.decode(video.data["thumb"], fit: BoxFit.cover))
                    child: Image.memory(base64.decode(snapshot.data[index].data["thumb"])),,
                  ),
                ),*/