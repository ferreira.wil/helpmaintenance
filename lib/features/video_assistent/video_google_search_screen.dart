import 'package:flutter/material.dart';
import 'package:help_maintenance/features/video_assistent/blocs/favorites_bloc.dart';
import 'package:help_maintenance/features/video_assistent/blocs/video_player_controller.dart';
import 'package:help_maintenance/features/video_assistent/video_tile_google.dart';


class VideoGooglesSearchScreen extends StatelessWidget {
  final videoSearchController;
  final favoritesSearchBlocs;

  VideoGooglesSearchScreen({Key key, this.videoSearchController, this.favoritesSearchBlocs}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar:  AppBar(
        backgroundColor: Color(0xFF1f345f),
        title: Center(child: Text("Pesquisa de videos")),
      ),
      body: ListFoundVideos(
      videoPlayer: videoSearchController, favoritesGoogle: favoritesSearchBlocs,),
    );
  }
}



class ListFoundVideos extends StatelessWidget {
  final videoPlayer;
  final favoritesGoogle;

  ListFoundVideos({Key key, this.videoPlayer, this.favoritesGoogle}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return StreamBuilder(
      stream: videoPlayer.outSearchVideos,
      builder: (context, snapshot) {
        print(snapshot.data);
        if (snapshot.hasData) {
          return ListView.builder(
            itemBuilder: (context, index) {
              if (index < snapshot.data.length) {
                return VideoTileGoogle(snapshot.data[index],favoritesGoogle);
              } else {
                return Container(
                  height: 40,
                  width: 40,
                  alignment: Alignment.center,
                  child: CircularProgressIndicator(
                    valueColor: AlwaysStoppedAnimation<Color>(Colors.red),
                  ),
                );
              }
            },
            itemCount: snapshot.data.length,
          );
        } else {
          return Center(child: CircularProgressIndicator());
        }
      },
    );
  }
}
