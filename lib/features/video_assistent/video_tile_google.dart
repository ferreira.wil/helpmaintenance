import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';
import 'dart:convert';
import 'package:url_launcher/url_launcher.dart';


class VideoTileGoogle extends StatelessWidget {
  final DocumentSnapshot video;
  final favoritesBlocs;

  VideoTileGoogle(this.video, this.favoritesBlocs);

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: ()async {
        print(video.data["url"]);
        var downloadUrl =
        video.data["url"];
        if (await canLaunch(downloadUrl)) {
          await launch(downloadUrl);
        } else {
          throw 'Could not launch $downloadUrl';
        }
      },
      child: Container(
        margin: EdgeInsets.symmetric(vertical: 4),
        child: Card(
          color: Color(0xFFE8E8E8),
          elevation: 10,
          child: InkWell(
            splashColor: Colors.blue.withAlpha(30),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.stretch,
              children: <Widget>[
                ClipRRect(
                  borderRadius: BorderRadius.circular(15),
                  child: AspectRatio(
                    aspectRatio: 16.0 / 9.0,
                    //MemoryImage(base64.decode(video.data["thumb"], fit: BoxFit.cover))
                    child: Image.memory(base64.decode(video.data["thumb"])),
                  ),
                ),
                Row(
                  children: <Widget>[
                    Expanded(
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: <Widget>[
                          Padding(
                            padding: EdgeInsets.fromLTRB(8, 8, 8, 0),
                            child: Text(
                              video.data["title"],
                              maxLines: 2,
                              style: TextStyle(fontSize: 16, fontWeight: FontWeight.bold,color: Color(0xFF5c88da)),
                            ),
                          ),
                        ],
                      ),
                    ),
                    StreamBuilder<Map<String, Map<String,String>>>(
                      stream: favoritesBlocs.outGoogleFav,
                      initialData: {},
                      builder: (context, snapshot) {
                        if(snapshot.hasData){
                          return IconButton(
                            icon: Icon(snapshot.data.containsKey(video.data["id"])?
                            Icons.star : Icons.star_border),
                            color: snapshot.data.containsKey(video.data["id"])? Colors.amberAccent : Colors.grey,
                            onPressed: (){
                              print("Snapshot.data: ${snapshot.data}");
                              favoritesBlocs.toggleGoogleFavorites(video);

                            },
                          );
                        }else{
                          return CircularProgressIndicator();
                        }
                      }
                    )
                  ],
                )
              ],
            ),
          ),
        ),
      ),
    );
  }
}


