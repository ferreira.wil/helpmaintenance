import 'dart:async';
import 'package:bloc_pattern/bloc_pattern.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/cupertino.dart';
import 'package:rxdart/rxdart.dart';

enum SearchState {IDLE, LOADING, SUCCESS, FAIL}

class SearchErrorsBloc implements BlocBase{


  final StreamController<Map<String, dynamic>> _errorsControler = BehaviorSubject<Map<String, dynamic>>();
  final StreamController<String> _searchError = BehaviorSubject<String>();
  final StreamController<SearchState>_searchState = BehaviorSubject<SearchState>();


  Stream<Map<String, dynamic>> get errorResult => _errorsControler.stream;
  Stream<String> get toSearch => _searchError.stream;
  Stream<SearchState> get SearchStatus => _searchState.stream;


  SearchErrorsBloc(){
    _searchState.stream.listen((value){
      print("Search State: $value");
    });
  }

  Function(String) get changeErrorField => _searchError.sink.add;
  List<DocumentSnapshot> errors;

  canSubmit(mode){
    _searchState.sink.add(SearchState.IDLE);


    _searchError.stream.listen((value){
      _searchState.sink.add(SearchState.LOADING);
      Stream<DocumentSnapshot> s = Firestore.instance.collection("errors").document(mode).collection("erro").document(value).snapshots();
      s.listen((value){
        if(value.data != null) {
          print(value.data);
          _errorsControler.sink.add(value.data);
          _searchState.sink.add(SearchState.SUCCESS);
        }
        else{
          _searchState.sink.add(SearchState.FAIL);
        }
      }
      );

    });
    _searchState.sink.add(SearchState.IDLE);

  }

  @override
  void dispose() {
    _searchError.close();
    _errorsControler.close();
    _searchState.close();
  }

}