import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

import 'blocs/search_erros_bloc.dart';

class ErrorsScreen extends StatelessWidget {
  static const id = "Errors_screen";
  final bool isRobo;

  ErrorsScreen({this.isRobo});

  final _searchErrorBloc = SearchErrorsBloc();



  Future<void> showAlertDialog(BuildContext context) async {
    return await showDialog(context: context,
        builder: (BuildContext context) {
          return AlertDialog(
            title: Text("Falha na pesquisa"),
            elevation: 5,
            content: Text("Não foi possível encontrar o erro digitado"),
            actions: <Widget>[
              FlatButton(
                child: Text("Pesquisar novamente"),
                onPressed: (){},
              )
            ],
          );
        });
  }


  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,
      appBar: AppBar(
        backgroundColor: Color(0xFF1f345f),
        title: isRobo
            ? Text('Códigos de Erro Robô Comau')
            : Text('Códigos de Erro Placa B&R'),
        centerTitle: true,
      ),
      body: SizedBox.expand(
        child: Container(
          decoration:  BoxDecoration(
            gradient: RadialGradient(
              center: Alignment.center,
              radius: 1, // 10% of the width, so there are ten blinds.
              colors: [
                const Color.fromRGBO(31, 52, 95, 1.0),
                const Color.fromRGBO(16, 27, 49, 1.0),
              ], // whitish to gray
            ),
          ),
          child: SingleChildScrollView(
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              crossAxisAlignment: CrossAxisAlignment.center,
              children: <Widget>[
                Padding(
                  padding: const EdgeInsets.all(15.0),
                  child: Image(
                    image: isRobo
                        ? AssetImage("images/comau_re_white.png")
                        : AssetImage("images/ber.png"),
                  ),
                ),
                SizedBox(
                  height: 40,
                ),
                StreamBuilder<SearchState>(
                  stream: _searchErrorBloc.SearchStatus,
                  initialData: SearchState.IDLE,
                  builder: (context,snapshot){
                    if(snapshot.hasData){
                      switch(snapshot.data) {
                        case SearchState.LOADING:
                          return Center(child: CircularProgressIndicator());
                          break;
                        case SearchState.SUCCESS:
                          return ShowResult(resultBloc: _searchErrorBloc.errorResult,);
                          break;
                        case SearchState.FAIL:
                        case SearchState.IDLE:
                          default:
                            return SearchField(bloc: _searchErrorBloc,isComau: isRobo,);
                      }
                    }else{
                      return SearchField(bloc: _searchErrorBloc,isComau: isRobo,);
                    }

                  },
                )

              ],
            ),
          ),
        ),
      ),
    );
  }
}

class ShowResult extends StatelessWidget {
  final resultBloc;

  ShowResult({Key key, this.resultBloc}) : super(key: key);

  var errorInfo;

  @override
  Widget build(BuildContext context) {
    return Container(
      child: Card(
        elevation: 10,
        child: Column(
          children: <Widget>[
            Padding(
              padding: EdgeInsets.all(8.0),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.center,
                crossAxisAlignment: CrossAxisAlignment.center,
                children: <Widget>[
                  Column(
                    children: <Widget>[
                      Text("Código de Alarme",
                      style: TextStyle(
                        color: Color(0xFF5c88da),
                        fontWeight: FontWeight.bold,
                        fontSize: 18
                      ),),
                      Padding(
                        padding: EdgeInsets.only(top: 5),
                        child: StreamBuilder<Object>(
                          stream: resultBloc,
                          builder: (context, snapshot) {
                            if(snapshot.hasData){
                              errorInfo = snapshot.data;
                              print(errorInfo);
                              return Text(errorInfo["code"],
                                style: TextStyle(fontSize: 18),
                              );
                            }
                            else{
                              return Center(child: CircularProgressIndicator());
                            }
                          }
                        ),
                      ),
                    ],
                  )
                ],
              ),
            ),
            Divider(
              color: Colors.blueGrey,
              thickness: 1,
              indent: 15,
              endIndent: 15,
            ),
            Padding(
              padding: EdgeInsets.all(8.0),
              child: Column(
                    mainAxisAlignment: MainAxisAlignment.center,
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: <Widget>[
                      Text(
                        "Descrição",
                        style: TextStyle(fontSize: 18,
                            color: Color(0xFF5c88da),
                            fontWeight: FontWeight.bold),
                      ),
                      Padding(
                        padding: EdgeInsets.only(top: 5),
                        child: StreamBuilder<Object>(
                            stream: resultBloc,
                            builder: (context, snapshot) {
                              if(snapshot.hasData){
                                errorInfo = snapshot.data;
                                print(errorInfo);
                                return Text(
                                  errorInfo["description"],
                                  style: TextStyle(fontSize: 14),
                                );
                              }
                              else{
                                return Center(child: CircularProgressIndicator());
                              }
                            }
                        ),
                      )
                    ],
                  ),
            ),
            Divider(
              color: Colors.blueGrey,
              thickness: 1,
              indent: 15,
              endIndent: 15,
            ),
            Padding(
              padding: EdgeInsets.all(8.0),
              child: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                crossAxisAlignment: CrossAxisAlignment.center,
                children: <Widget>[
                  Text(
                    "Tipo de dados",
                    style: TextStyle(fontSize: 18,
                        color: Color(0xFF5c88da),
                        fontWeight: FontWeight.bold),
                  ),
                  Padding(
                    padding: EdgeInsets.only(top: 5),
                    child: StreamBuilder<Object>(
                        stream: resultBloc,
                        builder: (context, snapshot) {
                          if(snapshot.hasData){
                            errorInfo = snapshot.data;
                            print(errorInfo);
                            return Text(
                              errorInfo["dataType"],
                              textAlign: TextAlign.start,
                              style: TextStyle(fontSize: 14),
                            );
                          }
                          else{
                            return Center(child: CircularProgressIndicator());
                          }
                        }
                    ),
                  )
                ],
              ),
            ),
            Divider(
              color: Colors.blueGrey,
              thickness: 1,
              indent: 15,
              endIndent: 15,
            ),
            Padding(
              padding: EdgeInsets.all(8.0),
              child: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                crossAxisAlignment: CrossAxisAlignment.center,
                children: <Widget>[
                  Text(
                    "Reação",
                    style: TextStyle(fontSize: 18,
                        color: Color(0xFF5c88da),
                        fontWeight: FontWeight.bold),
                  ),
                  Padding(
                    padding: EdgeInsets.only(top: 5),
                    child: StreamBuilder<Object>(
                        stream: resultBloc,
                        builder: (context, snapshot) {
                          if(snapshot.hasData){
                            errorInfo = snapshot.data;
                            print(errorInfo);
                            return Text(
                              errorInfo["reaction"],
                              textAlign: TextAlign.start,
                              style: TextStyle(fontSize: 14),
                            );
                          }
                          else{
                            return Center(child: CircularProgressIndicator());
                          }
                        }
                    ),
                  )
                ],
              ),
            ),
            Divider(
              color: Colors.blueGrey,
              thickness: 1,
              indent: 15,
              endIndent: 15,
            ),
            Padding(
              padding: EdgeInsets.all(8.0),
              child: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                crossAxisAlignment: CrossAxisAlignment.center,
                children: <Widget>[
                  Text(
                    "Possível Solução",
                    style: TextStyle(fontSize: 18,
                        color:  Color(0xFF5c88da),
                        fontWeight: FontWeight.bold),
                  ),
                  Padding(
                    padding: EdgeInsets.only(top: 5),
                    child: StreamBuilder<Object>(
                        stream: resultBloc,
                        builder: (context, snapshot) {
                          if(snapshot.hasData){
                            errorInfo = snapshot.data;
                            print(errorInfo);
                            return Text(
                              errorInfo["solutions"],
                              textAlign: TextAlign.start,
                              style: TextStyle(fontSize: 14),
                            );
                          }
                          else{
                            return Center(child: CircularProgressIndicator());
                          }
                        }
                    ),
                  )
                ],
              ),
            ),
            Divider(
              color: Colors.blueGrey,
              thickness: 1,
              indent: 15,
              endIndent: 15,
            ),
            Padding(
              padding: EdgeInsets.all(8.0),
              child: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                crossAxisAlignment: CrossAxisAlignment.center,
                children: <Widget>[
                  Padding(
                    padding: EdgeInsets.all(5.0),
                    child: Text(
                      "Avalie a Resposta",
                      style: TextStyle(fontSize: 18,
                          color:  Color(0xFF5c88da),
                          fontWeight: FontWeight.bold),
                    ),
                  ),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: <Widget>[

                      Card(
                          elevation: 5,
                          child: InkWell(
                            splashColor: Colors.blue.withAlpha(30),
                            onTap: () {
                              print('Card tapped.');
                              Navigator.pop(context);
                            },
                            child: IconButton(
                              icon: Icon(Icons.sentiment_satisfied,
                              color: Colors.green,),
                              onPressed: (){},
                            ),
                          ),
                        ),
                      Card(
                        elevation: 5,
                        child: InkWell(
                          splashColor: Colors.blue.withAlpha(30),
                          onTap: () {
                            print('Card tapped.');
                            Navigator.pop(context);
                          },
                          child: IconButton(
                            icon: Icon(Icons.sentiment_neutral,
                              color: Colors.amber,),
                            onPressed: (){},
                          ),
                        ),
                      ),
                      Card(
                        elevation: 5,
                        child: InkWell(
                          splashColor: Colors.blue.withAlpha(30),
                          onTap: () {
                            print('Card tapped.');
                            Navigator.pop(context);
                          },
                          child: IconButton(
                            icon: Icon(Icons.sentiment_dissatisfied,
                              color: Colors.red,),
                            onPressed: (){},
                          ),
                        ),
                      ),
                    ],
                  ),
                ],
              ),
            )
          ],
        ),
      ),

    );
  }
}


class SearchField extends StatelessWidget {
  final bloc;
  final isComau;

  const SearchField({Key key, this.bloc, this.isComau}) : super(key: key);


  @override
  Widget build(BuildContext context) {
    return Column(
      children: <Widget>[
        InputErrorField(
          errorStream: bloc.toSearch,
          onChangedError: bloc.changeErrorField,
        ),
        Padding(
          padding: EdgeInsets.symmetric(vertical: 40.0),
          child: StreamBuilder(
            stream: bloc.toSearch,
            builder: (context, snapshot) {
              return Material(
                color:  Color(0xFF5C88DA),
                borderRadius: BorderRadius.all(Radius.circular(30.0)),
                elevation: 5.0,
                child: MaterialButton(
                  onPressed: () {
                    if (snapshot.hasData) {
                      if (snapshot.data.length > 3) {
                        String mode;
                        if(isComau){
                          mode = "roboComau";

                        }else{
                          mode = "placaBeR";
                        }
                        bloc.canSubmit(mode);
                        return CircularProgressIndicator();
                      } else {
                        return null;
                      }
                    } else {
                      return null;
                    }
                  },
                  minWidth: 200.0,
                  height: 42.0,
                  child: Text("Investigar",style: TextStyle(color: Colors.white, fontSize: 20),),
                ),
              );
            },
          ),
        )
      ],
    );
  }
}

class InputErrorField extends StatelessWidget {
  final Stream<String> errorStream;
  final onChangedError;

  const InputErrorField(
      {Key key, @required this.errorStream, @required this.onChangedError})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return StreamBuilder(
      stream: errorStream,
      builder: (context, snapshot) {
        return Center(
          child: Container(
            width: 300,
            child: TextField(
              textAlign: TextAlign.center,
              keyboardType: TextInputType.number,
              onChanged: onChangedError,
              decoration: InputDecoration(
                filled: true,
                fillColor: Colors.white,
                hintText: "Digite o alarme encontrado",
                contentPadding:
                    EdgeInsets.symmetric(vertical: 10.0, horizontal: 20.0),
                border: OutlineInputBorder(
                  borderRadius: BorderRadius.all((Radius.circular(32.0))),
                ),
                enabledBorder: OutlineInputBorder(
                  borderRadius: BorderRadius.all((Radius.circular(32.0))),
                  borderSide: BorderSide(color: Color(0xFF6785c0), width: 1.0),
                ),
                focusedBorder: OutlineInputBorder(
                  borderRadius: BorderRadius.all((Radius.circular(32.0))),
                  borderSide: BorderSide(color: Color(0xFF6785c0), width: 2.0),
                ),
              ),
            ),
          ),
        );
      },
    );
  }
}
