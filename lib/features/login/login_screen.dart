import 'package:bloc_pattern/bloc_pattern.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/painting.dart';
import 'package:help_maintenance/features/home/home.dart';
import 'package:help_maintenance/features/login/blocs/login_bloc.dart';
import 'package:help_maintenance/features/user/user_bloc.dart';

class LoginScreen extends StatefulWidget {
  static const id = "Login_screen";
  @override
  _LoginScreenState createState() => _LoginScreenState();
}

class _LoginScreenState extends State<LoginScreen> {
  String email;
  String password;
  final _loginBloc = LoginBloc();

  @override
  void initState() {
    super.initState();

    _loginBloc.outState.listen((state) {
      switch (state) {
        case LoginState.SUCCESS:
          _loginBloc.outUser.listen((userData) {
            BlocProvider.of<UserBloc>(context).inData.add(userData);
            Navigator.of(context).pushReplacement(
                MaterialPageRoute(builder: (context) => HomeScreen()));
          });

          break;
        case LoginState.FAIL:
          showDialog(
              context: context,
              builder: (context) => AlertDialog(
                    title: Text("Erro",
                      style: TextStyle(
                          color:  Color(0xFF5c88da),
                          fontSize: 22),),
                    content: Text("As credenciais de acesso estão incorretas.",
                    style: TextStyle(
                        fontSize: 18
                    ),),
                elevation: 15,
                shape:RoundedRectangleBorder(borderRadius: BorderRadius.circular(15)),
                  ));
          break;
        case LoginState.LOADING:
          Padding(
            padding: EdgeInsets.symmetric(horizontal: 24.0),
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              crossAxisAlignment: CrossAxisAlignment.stretch,
              children: <Widget>[
                Container(
                  height: 200.0,
                  child: Image.asset('images/FCA_HM.png'),
                ),
                SizedBox(
                  height: 48.0,
                ),
                Center(
                  child: CircularProgressIndicator(
                    valueColor: AlwaysStoppedAnimation(Color(0xFFA1CDF4)),
                  ),
                ),
                SizedBox(
                  height: 24.0,
                ),
              ],
            ),
          );
          break;
        case LoginState.IDLE:
      }
    });
  }

  @override
  void dispose() {
    _loginBloc.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Color(0xFF1f345f),
      body: Container(
        decoration:  BoxDecoration(
          gradient: RadialGradient(
            center: Alignment.center,
            radius: 1, // 10% of the width, so there are ten blinds.
            colors: [
              const Color.fromRGBO(31, 52, 95, 1.0),
              const Color.fromRGBO(16, 27, 49, 1.0),
            ], // whitish to gray
          ),
        ),
        child: StreamBuilder<LoginState>(
            stream: _loginBloc.outState,
            initialData: LoginState.LOADING,
            builder: (context, snapshot) {
              print(snapshot.data);
              switch (snapshot.data) {
                case LoginState.LOADING:
                case LoginState.SUCCESS:
                  return Padding(
                    padding: EdgeInsets.symmetric(horizontal: 24.0),
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.center,
                      crossAxisAlignment: CrossAxisAlignment.stretch,
                      children: <Widget>[
                        Container(
                          height: 200.0,
                          child: Image.asset('images/FCA_HM.png'),
                        ),
                        SizedBox(
                          height: 48.0,
                        ),
                        Center(
                          child: CircularProgressIndicator(
                            valueColor: AlwaysStoppedAnimation(Color(0xFFA1CDF4)),
                          ),
                        ),
                        SizedBox(
                          height: 24.0,
                        ),
                      ],
                    ),
                  );
                case LoginState.IDLE:
                case LoginState.FAIL:
                default:
                  return Padding(
                    padding: EdgeInsets.symmetric(horizontal: 24.0),
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.center,
                      crossAxisAlignment: CrossAxisAlignment.stretch,
                      children: <Widget>[
                        Container(
                          height: 200.0,
                          child: Image.asset('images/FCA_HM.png'),
                        ),
                        SizedBox(
                          height: 48.0,
                        ),
                        InputField(
                          hint: "Digite seu Email",
                          obscure: false,
                          stream: _loginBloc.outEmail,
                          keyboard: TextInputType.emailAddress,
                          onChanged: _loginBloc.changeEmail,
                        ),
                        SizedBox(
                          height: 8.0,
                        ),
                        InputField(
                          hint: "Digite sua senha",
                          obscure: true,
                          stream: _loginBloc.outPassword,
                          keyboard: TextInputType.visiblePassword,
                          onChanged: _loginBloc.changePassword,
                        ),
                        SizedBox(
                          height: 24.0,
                        ),
                        Padding(
                          padding: EdgeInsets.symmetric(vertical: 16.0),
                          child: StreamBuilder(
                            stream: _loginBloc.outSubmitValid,
                            builder: (context, snapshot) {
                              return Material(
                                  color: Color(0xFF5C88DA),
                                  borderRadius:
                                      BorderRadius.all(Radius.circular(30.0)),
                                  elevation: 5.0,
                                  child: MaterialButton(
                                    onPressed: snapshot.hasData
                                        ? _loginBloc.submit
                                        : null,
                                    minWidth: 200.0,
                                    height: 42.0,
                                    child: Text("Entrar",style: TextStyle(fontSize: 20,
                                        color: Colors.white),),
                                  ));
                            },
                          ),
                        )
                      ],
                    ),
                  );
              }
            }),
      ),
    );
  }
}

class InputField extends StatelessWidget {
  final String hint;
  final bool obscure;
  final Stream<String> stream;
  final keyboard;
  final onChanged;

  InputField(
      {Key key,
      @required this.hint,
      @required this.obscure,
      this.stream,
      this.keyboard,
      this.onChanged});

  @override
  Widget build(BuildContext context) {
    return StreamBuilder<String>(
      stream: stream,
      builder: (context, snapshot) {
        return TextField(
          obscureText: obscure,
          keyboardType: keyboard,
          onChanged: onChanged,
          decoration: InputDecoration(
            errorText: snapshot.hasError ? snapshot.error : null,
            errorBorder: snapshot.hasError
                ? OutlineInputBorder(
                    borderSide: BorderSide(color: Colors.red, width: 2.0))
                : OutlineInputBorder(
                    borderSide:
                        BorderSide(color: Color.fromRGBO(103, 132, 192, 1.0), width: 2.0)),
            filled: true,
            fillColor: Color.fromRGBO(240, 240, 240, 1.0),
            hintText: hint,
            contentPadding:
                EdgeInsets.symmetric(vertical: 10.0, horizontal: 20.0),
            border: OutlineInputBorder(
              borderRadius: BorderRadius.all((Radius.circular(32.0))),
            ),
            enabledBorder: OutlineInputBorder(
              borderRadius: BorderRadius.all((Radius.circular(32.0))),
              borderSide: BorderSide(color: Color.fromRGBO(103, 132, 192, 1.0), width: 1.0),
            ),
            focusedBorder: OutlineInputBorder(
              borderRadius: BorderRadius.all((Radius.circular(32.0))),
              borderSide: BorderSide(color: Color.fromRGBO(103, 132, 192, 1.0), width: 2.0),
            ),
          ),
        );
      },
    );
  }
}
