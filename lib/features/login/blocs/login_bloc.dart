import 'dart:async';

import 'package:bloc_pattern/bloc_pattern.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:help_maintenance/features/login/blocs/validators.dart';
import 'package:rxdart/rxdart.dart';

enum LoginState {IDLE, LOADING, SUCCESS, FAIL}


class LoginBloc extends BlocBase with Validators{

  final _emailController = BehaviorSubject<String>();
  final _passwordController = BehaviorSubject<String>();
  final _stateController = BehaviorSubject<LoginState>();
  final _userController = BehaviorSubject<DocumentSnapshot>();

  Stream<String> get outEmail => _emailController.stream.transform(validateEmail);
  Stream<String> get outPassword => _passwordController.stream.transform(validatePassword);
  Stream<LoginState> get outState => _stateController.stream;
  Stream<DocumentSnapshot> get outUser => _userController.stream;

  Stream<bool> get outSubmitValid => Observable.combineLatest2(
      outEmail, outPassword, (email, password) => true
  );


  StreamSubscription _streamSubscription;

  Function(String) get changeEmail => _emailController.sink.add;
  Function(String) get changePassword => _passwordController.sink.add;



  LoginBloc(){
    _streamSubscription = FirebaseAuth.instance.onAuthStateChanged.listen((user){
      if (user != null){
        _stateController.add(LoginState.SUCCESS);
        getUserData(user);
      }else{
        _stateController.add(LoginState.IDLE);
      }
    });

  }



  Future<void> getUserData(loggedInUser) async {
    DocumentSnapshot userData = await Firestore.instance.collection("users").document(loggedInUser.uid).get();

    _userController.sink.add(userData);
  }

  void submit(){
    final email = _emailController.value;
    final password = _passwordController.value;

    _stateController.add(LoginState.LOADING);

      FirebaseAuth.instance.signInWithEmailAndPassword(email: email, password: password).catchError((error){
      var errorCode = error.code;
      var errorMessage = error.message;

      print("Codigo de erro $errorCode | Mensagem de erro $errorMessage");
      _stateController.add(LoginState.FAIL);

    });
  }



  @override
  void dispose() {
    _emailController.close();
    _passwordController.close();
    _stateController.close();
    _userController.close();
    _streamSubscription.cancel();
  }

}