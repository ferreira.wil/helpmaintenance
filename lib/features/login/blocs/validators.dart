import 'dart:async';

class Validators {

  static bool emailValidator(String value) {
    Pattern pattern =
        r'^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$';
    RegExp regex = RegExp(pattern);
    return (!regex.hasMatch(value)) ? false : true;
  }

  final validateEmail = StreamTransformer<String, String>.fromHandlers(handleData: (email, sink) {
    if (emailValidator(email)) {
      sink.add(email);
    } else {
      sink.addError("Insira um e-mail valido");
    }
  });



  final validatePassword = StreamTransformer<String, String>.fromHandlers(handleData: (password, sink) {
    if (password.length > 6) {
      sink.add(password);
    } else {
      sink.addError("Campo de senha deve conter pelo menos 6 digitos");
    }
  });

}
