import 'package:barcode_scan/barcode_scan.dart';
import 'package:camera/camera.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:help_maintenance/features/augmented_reality/blocs/qr_blocs.dart';
import 'package:help_maintenance/features/augmented_reality/camera_widget.dart';

class QRScannerWidget extends StatelessWidget {
  final CameraDescription camToUse;

  QRScannerWidget({Key key, this.camToUse}) : super(key: key);

  final _qrResult = QRBlocs();

  @override
  Widget build(BuildContext context) {
    return StreamBuilder(
      stream: _qrResult.outQRResult,
      builder: (context, snapshot) {
        if(snapshot.hasData){
          return CameraWidget(camera: camToUse,);

        }else{
          return Container(
            child: StreamBuilder(
              stream: _qrResult.outGrantedAccess,
              builder: (context,snapshot){
                if(snapshot.hasData){
                  print("HasData");
                  if(snapshot.data){
                    _qrResult.scanQR();
                    print("True - Granted");
                    return Container(
                      child: Text("Meio que rolou"),
                    );
                  }else{
                    print("false - Granted");
                    return Container(
                      child: Text("Meio n'ao que rolou"),
                    );
                  }
                }else{
                  print("Not granted");
                  return Center(child: CircularProgressIndicator(),);
                }
              },

            ),
          );
        }
      }
    );
  }
}
