import 'package:camera/camera.dart';
import 'package:flutter/material.dart';
import 'package:help_maintenance/features/augmented_reality/blocs/video_smp_blocs.dart';
import 'package:help_maintenance/features/search_errors/errors_screen.dart';
import 'package:url_launcher/url_launcher.dart';

import 'blocs/files_blocs.dart';
import 'blocs/options_blocs.dart';

//TODO: ESTE CODIGO PRECISA SER REESCRITO COM STATELESS WIDGTES

List<CameraDescription> cameras;

class CameraWidget extends StatefulWidget {
  final CameraDescription camera;

  const CameraWidget({Key key, this.camera}) : super(key: key);

  @override
  _CameraWidgetState createState() => _CameraWidgetState();
}

class _CameraWidgetState extends State<CameraWidget> {
  CameraController _controller;

  @override
  void initState() {
    super.initState();
    _controller = CameraController(widget.camera, ResolutionPreset.max);
    _controller.initialize().then((_) {
      if (!mounted) {
        return;
      }
      setState(() {});
    });
  }

  @override
  void dispose() {
    // Dispose of the controller when the widget is disposed.
    _controller.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    if (!_controller.value.isInitialized) {
      return Container(
        child: Center(child: CircularProgressIndicator()),
      );
    }
    return SingleChildScrollView(
      child: Container(
        child: LayersController(
          controller: _controller,
        ),
      ),
    );
  }
}

class LayersController extends StatelessWidget {
  final controller;
  final _helpButtonController = OptionsBlocs();
  LayersController({Key key, this.controller}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return StreamBuilder<Object>(
        stream: _helpButtonController.outHelpButton,
        builder: (context, snapshot) {
          if (snapshot.hasData) {
            if (snapshot.data == "Camera") {
              return Stack(
                children: <Widget>[
                  BackLayer(
                    controller: controller,
                    buttonController: _helpButtonController,
                  ),
                  Positioned(
                      bottom: 70,
                      left: 10,
                      child: SmpLayer()
                  ),
                ],
              );
            } else if (snapshot.data == "Search") {
              return Stack(
                children: <Widget>[
                  BackLayer(
                    controller: controller,
                    buttonController: _helpButtonController,
                  ),
                  Positioned(
                    bottom: 70,
                    right: 170,
                    child: Container(),
                  ),
                ],
              );
            } else if (snapshot.data == "File") {
              return Stack(
                children: <Widget>[
                  BackLayer(
                    controller: controller,
                    buttonController: _helpButtonController,
                  ),
                  Positioned(
                    bottom: 70,
                    right: 10,
                    child: FileLayer()
                  ),
                ],
              );
            } else {
              return CircularProgressIndicator();
            }
          } else {
            return BackLayer(
              controller: controller,
              buttonController: _helpButtonController,
            );
          }
        });
  }
}

class BackLayer extends StatelessWidget {
  final controller;
  final buttonController;

  BackLayer({Key key, this.controller, this.buttonController})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Column(
      children: <Widget>[
        Container(
          child: AspectRatio(
              aspectRatio: controller.value.aspectRatio,
              child: Container(child: CameraPreview(controller))),
        ),
        Row(
          mainAxisAlignment: MainAxisAlignment.start,
          crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[
            Expanded(
              child: Padding(
                padding: EdgeInsets.all(8.0),
                child: Material(
                  color: Color(0xFF1f345f),
                  borderRadius: BorderRadius.all(Radius.circular(50.0)),
                  child: InkWell(
                    splashColor: Colors.white,
                    child: IconButton(
                      icon: Icon(Icons.videocam, color: Colors.white),
                      onPressed: () {
                        buttonController.inHelpButton.add("Camera");
                      },
                    ),
                  ),
                ),
              ),
            ),
            Expanded(
              child: Padding(
                padding: EdgeInsets.all(8.0),
                child: Material(
                  color: Color(0xFF1f345f),
                  borderRadius: BorderRadius.all(Radius.circular(50.0)),
                  child: InkWell(
                    splashColor: Colors.white,
                    child: IconButton(
                      icon: Icon(Icons.search, color: Colors.white),
                      onPressed: () {
                        buttonController.inHelpButton.add("Search");
                        Navigator.push(context, MaterialPageRoute(builder: (context) => ErrorsScreen(isRobo: true,)));
                      },
                    ),
                  ),
                ),
              ),
            ),
            Expanded(
              child: Padding(
                padding: EdgeInsets.all(8.0),
                child: Material(
                  color: Color(0xFF1f345f),
                  borderRadius: BorderRadius.all(Radius.circular(50.0)),
                  child: InkWell(
                    splashColor: Colors.white,
                    child: IconButton(
                      icon: Icon(Icons.insert_drive_file, color: Colors.white),
                      onPressed: () {
                        buttonController.inHelpButton.add("File");
                      },
                    ),
                  ),
                ),
              ),
            )
          ],
        )
      ],
    );
  }
}

class SmpLayer extends StatelessWidget {
  final _smpController = VideosSMPBlocs();


  @override
  Widget build(BuildContext context) {
    return  Material(
      elevation: 15,
      borderRadius: BorderRadius.all(Radius.circular(10.0)),
      color: Color.fromRGBO(16, 27, 49, 0.8),
      child: Column(
        children: <Widget>[
          Text("SMP:",
              style: TextStyle(
                  color: Colors.white,
                  fontSize: 20,
                  fontWeight: FontWeight.bold)),
          Divider(
            thickness: 1,
            color: Colors.white,
            endIndent: 5,
          ),
          StreamBuilder(
              stream: _smpController.outSmp,
              builder: (context, snapshot) {
                if (snapshot.hasData) {
                  return SizedBox(
                    width: 350,
                    height: 275,
                    child: ListView.separated(
                      padding: EdgeInsets.all(5),
                      itemCount: snapshot.data.length,
                      separatorBuilder: (BuildContext context, int index) =>
                          Divider(
                            thickness: 0,
                          ),
                      itemBuilder: (BuildContext context, int index) {
                        return Card(
                            color: Color(0xFFf2f2f2),
                            elevation: 5,
                            child: ListTile(
                              leading: Text(
                                snapshot.data[index].data["title"],
                                style: TextStyle(
                                    color: Color(0xFF5c88da),
                                    fontSize: 18,
                                    fontWeight: FontWeight.bold),
                              ),
                              trailing: Icon(Icons.play_circle_filled),
                              onTap: () async {
                                print(snapshot.data[index].data["url"]);
                                var downloadUrl =
                                snapshot.data[index].data["url"];
                                if (await canLaunch(downloadUrl)) {
                                  await launch(downloadUrl);
                                } else {
                                  throw 'Could not launch $downloadUrl';
                                }
                              },
                            )
                        );
                      },
                    ),
                  );
                } else {
                  return CircularProgressIndicator();
                }
              }),
        ],
      ),
    );
  }
}


class FileLayer extends StatelessWidget {
  final _filesController = FilesBlocs();

  @override
  Widget build(BuildContext context) {
    return Material(
      elevation: 15,
      borderRadius: BorderRadius.all(Radius.circular(10.0)),
      color: Color.fromRGBO(16, 27, 49, 0.8),
      child: Column(
        children: <Widget>[
          Text("Alarme:",
              style: TextStyle(
                  color: Colors.white,
                  fontSize: 20,
                  fontWeight: FontWeight.bold)),
          Divider(
            thickness: 1,
            color: Colors.white,
            endIndent: 5,
          ),
          StreamBuilder(
              stream: _filesController.outDocsResult,
              builder: (context, snapshot) {
                if (snapshot.hasData) {
                  return SizedBox(
                    width: 350,
                    height: 275,
                    child: ListView.separated(
                      padding: EdgeInsets.all(5),
                      itemCount: snapshot.data.length,
                      separatorBuilder: (BuildContext context, int index) =>
                          Divider(
                            thickness: 0,
                          ),
                      itemBuilder: (BuildContext context, int index) {
                        return Card(
                            color: Color(0xFFf2f2f2),
                            elevation: 5,
                            child: ListTile(
                              leading: Text(
                                snapshot.data[index].data["title"] + " - " + snapshot.data[index].data["desc"],
                                style: TextStyle(
                                    color: Color(0xFF5c88da),
                                    fontSize: 18,
                                    fontWeight: FontWeight.bold),
                              ),
                              trailing: Icon(Icons.file_download),
                              onTap: () async {
                                print(snapshot.data[index].data["url"]);
                                var downloadUrl =
                                snapshot.data[index].data["url"];
                                if (await canLaunch(downloadUrl)) {
                                  await launch(downloadUrl);
                                } else {
                                  throw 'Could not launch $downloadUrl';
                                }
                              },
                            )
                        );
                      },
                    ),
                  );
                } else {
                  return CircularProgressIndicator();
                }
              }),
        ],
      ),
    );
  }
}





