import 'dart:async';

import 'package:bloc_pattern/bloc_pattern.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_storage/firebase_storage.dart';
import 'package:rxdart/rxdart.dart';

class VideosSMPBlocs extends BlocBase{

  StreamController<List<DocumentSnapshot>> _smpResults = BehaviorSubject<List<DocumentSnapshot>>();


  Stream<List<DocumentSnapshot>> get outSmp => _smpResults.stream;


  VideosSMPBlocs(){
    listVideosSMP();
    
  }

  listVideosSMP() async {
    Stream<QuerySnapshot> videossmp = await Firestore.instance.collection("videos").document("smp").collection("smp").snapshots();

    videossmp.listen((event) {

      _smpResults.sink.add(event.documents);
    });


  }

  @override
  void dispose() {
    // TODO: implement dispose

    _smpResults.close();
  }

}