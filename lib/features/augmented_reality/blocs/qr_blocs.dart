
import 'dart:async';

import 'package:barcode_scan/barcode_scan.dart';
import 'package:bloc_pattern/bloc_pattern.dart';
import 'package:flutter/services.dart';
import 'package:rxdart/rxdart.dart';

class QRBlocs extends BlocBase{

  StreamController<String> _qrResultController = BehaviorSubject<String>();
  StreamController<bool> _camAccess = BehaviorSubject<bool>();


  Stream get outQRResult => _qrResultController.stream;
  Stream get outGrantedAccess => _camAccess.stream;

  QRBlocs(){
    if(BarcodeScanner.cameraAccessGranted == 'PERMISSION_GRANTED'){
      _camAccess.add(true);
    }else{
      _camAccess.add(false);
    }

  }

  scanQR() async{
    ScanResult qrResult;
    try {
       qrResult = await BarcodeScanner.scan();

    } on PlatformException catch(e){
      _qrResultController.sink.addError(e);
    }

    if(qrResult != null){
      _qrResultController.sink.add(qrResult.rawContent);
      print(qrResult.rawContent);
    }

  }

  @override
  void dispose() {
    // TODO: implement dispose
    _camAccess.close();
    _qrResultController.close();
  }

}