import 'dart:async';

import 'package:bloc_pattern/bloc_pattern.dart';
import 'package:flutter/material.dart';
import 'package:rxdart/rxdart.dart';

class OptionsBlocs extends BlocBase{

  StreamController<String> _helpButtonController = BehaviorSubject<String>();


  OptionsBlocs(){
    outHelpButton.listen((event) {
      print(event);
    });
  }


  Sink get inHelpButton =>  _helpButtonController.sink;
  Stream get outHelpButton => _helpButtonController.stream;




  @override
  void dispose() {
    // TODO: implement dispose
    _helpButtonController.close();
  }


}