import 'dart:async';

import 'package:bloc_pattern/bloc_pattern.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_storage/firebase_storage.dart';
import 'package:rxdart/rxdart.dart';

class FilesBlocs extends BlocBase{


  StreamController<List<DocumentSnapshot>> _docsResult = BehaviorSubject<List<DocumentSnapshot>>();


  Stream<List<DocumentSnapshot>> get outDocsResult => _docsResult.stream;


  FilesBlocs() {
    listDocs();
  }

  listDocs()async{
    Stream<QuerySnapshot> docs = await Firestore.instance.collection("troubleshooting").document("Comau").collection("c5g").snapshots();

    docs.listen((event) {
      _docsResult.sink.add(event.documents);
    });


  }


  @override
  void dispose() {
    // TODO: implement dispose

    _docsResult.close();
  }
}