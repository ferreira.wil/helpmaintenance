
import 'package:camera/camera.dart';
import 'package:flutter/material.dart';
import 'package:help_maintenance/features/augmented_reality/camera_widget.dart';
import 'package:help_maintenance/features/augmented_reality/qr_scan.dart';




class CameraScreen extends StatelessWidget {
  final List<CameraDescription> cameras;
  final String title;

  const CameraScreen({Key key, this.cameras, this.title}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Color(0xFFf0f0f0),
      appBar: AppBar(
        backgroundColor:  Color(0xFF1f345f),
        title: Padding(
          padding: EdgeInsets.only(left: 35),
          //child: Text("Escaneie o QR Code",style: TextStyle(fontSize: 20),),
        ),
      ),
      body: Container(
        child: QRScannerWidget(camToUse: cameras[0],),
        //child: CameraWidget(camera: cameras[0],),
      ),
    );
  }
}

