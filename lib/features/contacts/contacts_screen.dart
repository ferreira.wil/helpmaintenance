import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:help_maintenance/features/contacts/blocs/contacts_bloc.dart';
import 'package:help_maintenance/features/chat/chat_screen.dart';
import 'package:url_launcher/url_launcher.dart';

class ContactScreen extends StatelessWidget {
  //TODO: USAR O USER BLOC AQUI
  static const id = "Contact_screen";
  final DocumentSnapshot user;

  ContactScreen({Key key, @required this.user});

  final _contactBloc = ContactBloc();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,
      appBar: AppBar(
        backgroundColor: Color(0xFF1f345f),
        title: Text("Lista de contatos"),
        centerTitle: true,
      ),
      body: StreamBuilder(
        stream: _contactBloc.outContacts,
        builder: (context, snapshot) {
          if (snapshot.hasData) {
            return ListView.separated(
              padding: const EdgeInsets.all(8),
              separatorBuilder: (BuildContext context, int index) =>
                  const Divider(
                color: Colors.white,
              ),
              itemCount: snapshot.data.length,
              itemBuilder: (BuildContext context, int index) {
                return Container(
                  height: 110,
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.center,
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: <Widget>[
                      Row(
                        mainAxisAlignment: MainAxisAlignment.center,
                        crossAxisAlignment: CrossAxisAlignment.center,
                        children: <Widget>[
                          Expanded(
                            child: Card(
                              color: Color(0xFFf2f2f2),
                              elevation: 5,
                              child: InkWell(
                                  splashColor: Colors.blue.withAlpha(30),
                                  onTap: () {
                                    print('Card tapped.');
                                  },
                                  child: Padding(
                                    padding: EdgeInsets.fromLTRB(5, 0, 0, 0),
                                    child: Container(
                                      height: 100,
                                      child: Row(
                                        children: <Widget>[
                                          Column(
                                            crossAxisAlignment:
                                                CrossAxisAlignment.start,
                                            mainAxisAlignment:
                                                MainAxisAlignment.start,
                                            children: <Widget>[
                                              Row(
                                                mainAxisAlignment:
                                                    MainAxisAlignment.start,
                                                crossAxisAlignment:
                                                    CrossAxisAlignment.start,
                                                children: <Widget>[
                                                  Text(
                                                    snapshot.data[index]
                                                            .data["nome"] +
                                                        " " +
                                                        snapshot.data[index]
                                                            .data["sobrenome"],
                                                    style: TextStyle(
                                                        fontSize: 20,
                                                        fontFamily: "Roboto",
                                                        color: Color(0xFF5c88da)),
                                                  ),
                                                ],
                                              ),
                                              Row(
                                                mainAxisAlignment:
                                                    MainAxisAlignment.start,
                                                crossAxisAlignment:
                                                    CrossAxisAlignment.start,
                                                children: <Widget>[
                                                  Text(
                                                    "Área: " + snapshot.data[index].data["area"],
                                                    style: TextStyle(
                                                      fontSize: 14,
                                                      fontFamily: "Roboto",
                                                        color: Color(0xFF898C8A)
                                                    ),
                                                  ),
                                                ],
                                              ),
                                              Row(
                                                mainAxisAlignment:
                                                    MainAxisAlignment.start,
                                                crossAxisAlignment:
                                                    CrossAxisAlignment.start,
                                                children: <Widget>[
                                                  Text(
                                                    "Setor: " +
                                                        snapshot.data[index]
                                                            .data["setor"],
                                                    style: TextStyle(
                                                      fontSize: 14,
                                                      fontFamily: "Roboto",
                                                        color: Color(0xFF898C8A)
                                                    ),
                                                  ),
                                                ],
                                              ),
                                              Row(
                                                mainAxisAlignment:
                                                    MainAxisAlignment.start,
                                                crossAxisAlignment:
                                                    CrossAxisAlignment.start,
                                                children: <Widget>[
                                                  Text(
                                                    "Telefone: " +
                                                        snapshot.data[index]
                                                            .data["telefone"],
                                                    style: TextStyle(
                                                      fontSize: 14,
                                                      fontFamily: "Roboto",
                                                        color: Color(0xFF898C8A)
                                                    ),
                                                  ),
                                                ],
                                              ),
                                              Row(
                                                mainAxisAlignment:
                                                    MainAxisAlignment.start,
                                                crossAxisAlignment:
                                                    CrossAxisAlignment.start,
                                                children: <Widget>[
                                                  Text(
                                                    "Email: " +
                                                        snapshot.data[index]
                                                            .data["email"],
                                                    style: TextStyle(
                                                      fontSize: 14,
                                                      fontFamily: "Roboto",
                                                        color: Color(0xFF898C8A)
                                                    ),
                                                  ),
                                                ],
                                              ),
                                            ],
                                          ),
                                          Expanded(
                                            child: Column(
                                              crossAxisAlignment:
                                                  CrossAxisAlignment.end,
                                              mainAxisAlignment:
                                                  MainAxisAlignment.end,
                                              children: <Widget>[
                                                Row(
                                                  mainAxisAlignment:
                                                      MainAxisAlignment.end,
                                                  crossAxisAlignment:
                                                      CrossAxisAlignment.end,
                                                  children: <Widget>[
                                                    IconButton(
                                                      icon: Icon(
                                                          Icons.video_call),
                                                      iconSize: 20,
                                                      color: Color(0xFF564256),
                                                      onPressed: () async {
                                                        if (await canLaunch("tel:" +
                                                            snapshot.data[index]
                                                                    .data[
                                                                "telefone"])) {
                                                          await launch("tel:" +
                                                              snapshot
                                                                      .data[index]
                                                                      .data[
                                                                  "telefone"]);
                                                        } else {
                                                          throw 'Could not launch';
                                                        }
                                                      },
                                                    ),
                                                    IconButton(
                                                      icon:
                                                          Icon(Icons.add_call),
                                                      iconSize: 20,
                                                      color: Color(0xFF564256),
                                                      onPressed: () async {
                                                        if (await canLaunch("tel:" +
                                                            snapshot.data[index]
                                                                    .data[
                                                                "telefone"])) {
                                                          await launch("tel:" +
                                                              snapshot
                                                                      .data[index]
                                                                      .data[
                                                                  "telefone"]);
                                                        } else {
                                                          throw 'Could not launch';
                                                        }
                                                      },
                                                    ),
                                                    IconButton(
                                                      icon: Icon(Icons.chat),
                                                      iconSize: 20,
                                                      color: Color(0xFF564256),
                                                      onPressed: () {
                                                        Navigator.push(
                                                            context,
                                                            MaterialPageRoute(
                                                                builder:
                                                                    (context) =>
                                                                        ChatScreen(
                                                                          user:
                                                                              user,
                                                                          contact:
                                                                              snapshot.data[index],
                                                                        )));
                                                      },
                                                    ),
                                                  ],
                                                ),
                                              ],
                                            ),
                                          )
                                        ],
                                      ),
                                    ),
                                  )),
                            ),
                          )
                        ],
                      )
                    ],
                  ),
                );
              },
            );
          } else {
            return Center(
                child: CircularProgressIndicator(
              valueColor: AlwaysStoppedAnimation<Color>(Colors.lightBlue),
            ));
          }
        },
      ),
    );
  }
}
