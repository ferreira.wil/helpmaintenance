import 'dart:async';
import 'package:bloc_pattern/bloc_pattern.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:rxdart/rxdart.dart';

class ContactBloc extends BlocBase{

  final StreamController<List<DocumentSnapshot>> _contactsController = BehaviorSubject<List<DocumentSnapshot>>();

  Stream get outContacts => _contactsController.stream;

  ContactBloc(){
    _getContacts();
  }

  List<DocumentSnapshot> userContacts;

  void _getContacts() async{
    QuerySnapshot snapshot = await Firestore.instance.collection("users").getDocuments();

    userContacts = snapshot.documents;

    _contactsController.sink.add(userContacts);
  }


  @override
  void dispose() {
    _contactsController.close();
  }

}