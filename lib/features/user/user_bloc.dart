import 'dart:async';
import 'package:bloc_pattern/bloc_pattern.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:rxdart/rxdart.dart';

class UserBloc extends BlocBase{

  final StreamController<DocumentSnapshot> _userController = BehaviorSubject<DocumentSnapshot>();
  final StreamController<DocumentSnapshot> _dataController = BehaviorSubject<DocumentSnapshot>();



  Stream<DocumentSnapshot> get outUser => _userController.stream.asBroadcastStream();
  Sink get inData => _dataController.sink;


  UserBloc() {

    _dataController.stream.listen((data){
      _userController.sink.add(data);
    });
  }

  @override
  void dispose() {
    _dataController.close();
    _userController.close();
  }

}

