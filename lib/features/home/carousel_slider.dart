import 'package:carousel_slider/carousel_slider.dart';
import 'package:flutter/material.dart';
import 'package:url_launcher/url_launcher.dart';

class Carousel extends StatefulWidget {
  @override
  _CarouselState createState() => _CarouselState();
}

class _CarouselState extends State<Carousel> {
  int _current = 0;

  @override
  Widget build(BuildContext context) {
    return Stack(
      children: <Widget>[
        CarouselSlider(
          autoPlay: true,
          autoPlayInterval: Duration(seconds: 3),
          height: 200,
          aspectRatio: 16 / 9,
          items: child,
          onPageChanged: (index) {
            setState(() {
              _current = index;
            });
          },
        ),
        Positioned(
            bottom: 0.0,
            left: 0.0,
            right: 0.0,
            child: Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: map<Widget>(imgList, (index, url) {
                return Container(
                  width: 8.0,
                  height: 8.0,
                  margin: EdgeInsets.symmetric(vertical: 10.0, horizontal: 2.0),
                  decoration: BoxDecoration(
                      shape: BoxShape.circle,
                      color: _current == index
                          ? Color.fromRGBO(0, 0, 0, 0.9)
                          : Color.fromRGBO(0, 0, 0, 0.4)),
                );
              }),
            ))
      ],
    );
  }
}

final List child = map<Widget>(
  imgList,
  (index, i) {
    return Container(
      margin: EdgeInsets.all(5.0),
      child: ClipRRect(
        borderRadius: BorderRadius.all(Radius.circular(5.0)),
        child: Stack(children: <Widget>[
          GestureDetector(
            child: ClipRRect(
              borderRadius: BorderRadius.circular(8),
                child: Image.asset(i, fit: BoxFit.cover, width: 1000.0)),
            onTap: () async {
              var url  = urlList[index];
              if (await canLaunch(url)) {
                await launch(url,forceWebView: true);
              } else {
                throw 'Could not launch $url';
              }
            },
          ),
          Positioned(
            top: 0.0,
            left: 0.0,
            right: 0.0,
            child: Container(
              decoration: BoxDecoration(
                gradient: LinearGradient(
                  colors: [
                    Color.fromARGB(0, 0, 0, 0),
                    Color.fromARGB(200, 0, 0, 0)
                  ],
                  begin: Alignment.bottomCenter,
                  end: Alignment.topCenter,
                ),
              ),
              padding: EdgeInsets.symmetric(vertical: 10.0, horizontal: 20.0),
              child: Text(
                textList[index],
                style: TextStyle(
                  color: Colors.white,
                  fontSize: 20.0,
                  fontWeight: FontWeight.bold,
                ),
              ),
            ),
          ),
        ]),
      ),
    );
  },
).toList();



final List<String> urlList = [
  'https://www.fcagroup.com/en-US/media_center/insights/pages/new_fiat_500_electric.aspx',
  'https://www.fcagroup.com/en-us/media_center/insights/pages/turin_manufacturing_hub.aspx',
  'https://www.fcagroup.com/en-US/media_center/insights/Pages/fca_coronavirus_relief_actions.aspx'
];

final List<String> textList = [
  'Novo 500',
  'Fotovoltaicas em Turim',
  'FCA Contra COVID-19'
];

final List<String> imgList = [
  'images/FIAT_500.png',
  'images/SOLAR.png',
  'images/FCA_DOCTORS.png'
];

List<T> map<T>(List list, Function handler) {
  List<T> result = [];
  for (var i = 0; i < list.length; i++) {
    result.add(handler(i, list[i]));
  }

  return result;
}
