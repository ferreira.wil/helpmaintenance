
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:help_maintenance/features/home/carousel_slider.dart';
import 'package:help_maintenance/features/login/login_screen.dart';

import 'navigation_bar.dart';

final List<String> errors = <String>['A', 'B', 'C'];

class HomeScreen extends StatelessWidget {
  static const id = "Home_screen";




  @override
  Widget build(BuildContext context) {

    return Scaffold(
        backgroundColor: Colors.white,
        appBar: AppBar(
          backgroundColor: Color.fromRGBO(31, 52, 95, 1.0),
          title: Text("Assistente de Manutenção"),
          centerTitle: true,
          actions: <Widget>[
            IconButton(
              icon: Icon(Icons.exit_to_app),
              onPressed: () {
                FirebaseAuth.instance.signOut();
                Navigator.of(context).pushReplacement(MaterialPageRoute( builder: (context) => LoginScreen()));
              },
            )
          ],
        ),
        drawer: NavBar(),
        body: SingleChildScrollView(
          child: Column(
            children: <Widget>[
              Padding(
                padding: EdgeInsets.only(top: 10.0),
                child: Carousel(),
              ),
              Padding(
                padding: EdgeInsets.all(10.0),
                child:Card(
                  color: Color.fromRGBO(31, 52, 95, 1.0),
                  shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(15.0)
                  ),
                  elevation: 5,
                  child: Column(
                    children: <Widget>[
                      Row(
                        mainAxisAlignment: MainAxisAlignment.start,
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: <Widget>[
                          Container(
                            padding: EdgeInsets.only(left: 15,top: 5,bottom: 10),
                            child: Text(
                              "Últimos contatos",
                              style: TextStyle(
                                fontWeight: FontWeight.bold,
                                fontSize: 20,
                                color: Colors.white,
                              ),
                            ),
                          ),
                        ],
                      ),
                      SizedBox(
                        height: 230,
                        child: ListView(
                          children: <Widget>[
                            Card(
                              child: ListTile(
                                leading: Icon(Icons.account_circle),
                                title: Text('Felipe Menezes',
                                style: TextStyle(
                                    color: Color(0xFF5c88da),
                                ),),
                                trailing: Icon(Icons.arrow_forward),
                              ),
                            ),
                            Card(
                              child: ListTile(
                                leading: Icon(Icons.account_circle),
                                title: Text('Deangellis Santiago',
                                  style: TextStyle(
                                  color: Color(0xFF5c88da),
                                ),),
                                trailing: Icon(Icons.arrow_forward),
                              ),
                            ),
                            Card(
                              child: ListTile(
                                leading: Icon(Icons.account_circle),
                                title: Text('Roberto Lobato',
                                style: TextStyle(
                                  color: Color(0xFF5c88da),
                                ),),
                                trailing: Icon(Icons.arrow_forward),
                              ),
                            ),
                            Card(
                              child: ListTile(
                                leading: Icon(Icons.account_circle),
                                title: Text('Willer Reis',
                                style: TextStyle(
                                  color: Color(0xFF5c88da),
                                ),),
                                trailing: Icon(Icons.arrow_forward),
                              ),
                            ),
                          ],
                        ),
                      ),
                      SizedBox(
                        height: 10,
                      )
                    ],
                  ),
                ),
              ),
              Padding(
                  padding: EdgeInsets.all(10.0),
                  child: Card(
                    color: Color.fromRGBO(31, 52, 95, 1.0),
                    shape: RoundedRectangleBorder(
                        borderRadius: BorderRadius.circular(15.0)
                    ),
                    elevation: 5,
                    child: Column(
                      children: <Widget>[
                        Row(
                          children: <Widget>[
                            Container(
                              padding: EdgeInsets.only(left: 15,top: 5,bottom: 10),
                              child: Text(
                                "Últimas pesquisas",
                                style: TextStyle(
                                  fontSize: 20,
                                  fontWeight: FontWeight.bold,
                                  color: Colors.white,
                                ),
                              ),
                            ),
                          ],
                        ),
                        SizedBox(
                            height: 220.0,
                            child: ListView(
                              children: <Widget>[
                                Card(
                                  child: ListTile(
                                    leading: Icon(Icons.android),
                                    title: Text('Erro Robô 5000',
                                    style: TextStyle(
                                      color: Color(0xFF5c88da),
                                    ),),
                                    trailing: Icon(Icons.arrow_forward),
                                  ),
                                ),
                                Card(
                                  child: ListTile(
                                    leading: Icon(Icons.error),
                                    title: Text('Erro de Placa 4658',
                                    style: TextStyle(
                                      color: Color(0xFF5c88da),
                                    ),),
                                    trailing: Icon(Icons.arrow_forward),
                                  ),
                                ),
                                Card(
                                  child: ListTile(
                                    leading: Icon(Icons.error),
                                    title: Text('Erro de Placa 1006',
                                    style: TextStyle(
                                      color: Color(0xFF5c88da),
                                    ),),
                                    trailing: Icon(Icons.arrow_forward),
                                  ),
                                ),
                                Card(
                                  child: ListTile(
                                    leading: Icon(Icons.android),
                                    title: Text('Erro Robô 4981',
                                    style: TextStyle(
                                      color: Color(0xFF5c88da),
                                    ),),
                                    trailing: Icon(Icons.arrow_forward),
                                  ),
                                ),
                                Card(
                                  child: ListTile(
                                    leading: Icon(Icons.error),
                                    title: Text('Erro de Placa 3597',
                                    style: TextStyle(
                                      color: Color(0xFF5c88da),
                                    ),),
                                    trailing: Icon(Icons.arrow_forward),
                                  ),
                                ),
                              ],
                            )
                        ),
                        SizedBox(
                          height: 10,
                        )
                      ],
                    ),
                  ),
              ),
            ],
          ),
        ));
  }
}


