import 'dart:typed_data';
import 'dart:convert';
import 'package:bloc_pattern/bloc_pattern.dart';
import 'package:camera/camera.dart';
import 'package:flutter/material.dart';
import 'package:help_maintenance/features/augmented_reality/camera_screen.dart';
import 'package:help_maintenance/features/contacts/contacts_screen.dart';
import 'package:help_maintenance/features/search_errors/errors_screen.dart';
import 'package:help_maintenance/features/user/user_bloc.dart';
import 'package:help_maintenance/features/video_assistent/video_player_screen.dart';
import 'package:help_maintenance/features/video_call/video_call_screen.dart';


enum Status { Online, Busy, Out, Offline }

var iconStatus = Icons.check_circle;
var iconColor = Colors.green;

class NavBar extends StatefulWidget {




  @override
  _NavBarState createState() => _NavBarState();
}

class _NavBarState extends State<NavBar> {



  var loggedUser;
  var userName =  Text("");
  var userEmail = Text("");
  Uint8List userPhoto =new Uint8List(10);


  Future<void> OpenDialog() async {
    switch (await showDialog(
        context: context,
        builder: (BuildContext context) {
          return SimpleDialog(
            elevation: 15,
            shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(15)),
            title: Text("Alterar Status",
            style: TextStyle(
              color:  Color(0xFF5c88da),
              fontSize: 22,
            ),),
            children: <Widget>[
              Padding(
                padding: EdgeInsets.all(10),
                child: SimpleDialogOption(
                  onPressed: () {
                    Navigator.pop(context, Status.Online);
                  },
                  child: Row(
                    children: <Widget>[
                      Padding(
                        padding: EdgeInsets.fromLTRB(0, 0, 120, 0),
                        child: Text("Disponível",
                        style: TextStyle(
                          fontSize: 18,
                          fontWeight: FontWeight.w600
                        ),),
                      ),
                      Icon(
                        Icons.check_circle,
                        color: Colors.green,
                      ),
                    ],
                  ),
                ),
              ),
              Padding(
                padding: EdgeInsets.all(10),
                child: SimpleDialogOption(
                  onPressed: () {
                    Navigator.pop(context, Status.Busy);
                  },
                  child: Row(
                    children: <Widget>[
                      Padding(
                        padding: EdgeInsets.fromLTRB(0, 0, 120, 0),
                        child: Text("Ocupado  ",style:TextStyle(
                            fontSize: 18,
                            fontWeight: FontWeight.w600
                        ),),
                      ),
                      Icon(
                        Icons.do_not_disturb_on,
                        color: Colors.red,
                      )
                    ],
                  ),
                ),
              ),
              Padding(
                padding: EdgeInsets.all(10),
                child: SimpleDialogOption(
                  onPressed: () {
                    Navigator.pop(context, Status.Out);
                  },
                  child: Row(
                    children: <Widget>[
                      Padding(
                        padding: EdgeInsets.fromLTRB(0, 0,120, 0),
                        child: Text("Ausente   ",style: TextStyle(
                            fontSize: 18,
                            fontWeight: FontWeight.w600
                        ),),
                      ),
                      Icon(
                        Icons.access_time,
                        color: Colors.yellow,
                      )
                    ],
                  ),
                ),
              ),
              Padding(
                padding: EdgeInsets.all(10),
                child: SimpleDialogOption(
                  onPressed: () {
                    Navigator.pop(context, Status.Offline);
                  },
                  child: Row(
                    children: <Widget>[
                      Padding(
                        padding: EdgeInsets.fromLTRB(0, 0, 120, 0),
                        child: Text("Offline      ",style:
                        TextStyle(
                            fontSize: 18,
                            fontWeight: FontWeight.w600
                        ),),
                      ),
                      Icon(
                        Icons.block,
                        color: Colors.blueGrey,
                      )
                    ],
                  ),
                ),
              ),
            ],
          );
        })) {
      case Status.Online:
        setState(() {
          iconStatus = Icons.check_circle;
          iconColor = Colors.green;
        });
        break;
      case Status.Busy:
        setState(() {
          iconStatus = Icons.do_not_disturb_on;
          iconColor = Colors.red;
        });
        break;
      case Status.Out:
        setState(() {
          iconStatus = Icons.access_time;
          iconColor = Colors.yellow;
        });
        break;
      case Status.Offline:
        setState(() {
          iconStatus = Icons.block;
          iconColor = Colors.blueGrey;
        });
        break;
    }
  }

  @override
  Widget build(BuildContext context) {
    return Drawer(
      child: Container(
        decoration: BoxDecoration(
          gradient: LinearGradient(
            begin: Alignment.topLeft,
            end: Alignment.bottomRight,
            colors: [
              const Color.fromRGBO(31, 52, 95, 1.0),
              const Color.fromRGBO(16, 27, 49, 1.0),
            ], // whitish to gray
          ),
        ),
        child: ListView(
          children: <Widget>[
            StreamBuilder(
              stream: BlocProvider.of<UserBloc>(context).outUser,
              builder: (context, snapshot){
                loggedUser = snapshot.data;
                if(snapshot.hasData){
                  return UserAccountsDrawerHeader(
                    accountName:  Text(snapshot.data["nome"] + " " +snapshot.data["sobrenome"],style: TextStyle(color: Color(0xFF5c88da),fontSize: 18 )),
                    accountEmail: Text(snapshot.data["email"],style: TextStyle(color: Color(0xFF898C8A),fontSize: 18 )),

                    currentAccountPicture: Stack(
                      children: <Widget>[
                        CircleAvatar(
                          backgroundColor: Colors.grey,
                          radius: 65,
                          backgroundImage: MemoryImage(base64.decode(snapshot.data["foto"])),
                        ),
                        Padding(
                          padding: EdgeInsets.fromLTRB(35, 45, 0, 0),
                          child: IconButton(
                            icon: Icon(
                              iconStatus,
                              color: iconColor,
                            ),
                            onPressed: () {
                              OpenDialog();
                            },
                          ),
                        )
                      ],
                    ),
                    decoration: BoxDecoration(color:Color(0xFFf0f0f0)),
                  );
                }else{
                  return UserAccountsDrawerHeader(
                    accountName:  Text("Usuario nao identificado - Contate o suporte",style: TextStyle(color: Color(0xFF5c88da) ),),
                    accountEmail: Text("suporte@neuco.com.br"),

                    currentAccountPicture: Stack(
                      children: <Widget>[
                        CircleAvatar(
                          backgroundColor: Colors.grey,
                          radius: 65,
                        ),
                      ],
                    ),
                    decoration: BoxDecoration(color:Color(0xFFf0f0f0)),
                  );
                }
              },
            ),
            ListTile(
              leading: Icon(Icons.error,color: Colors.white,),
              title: Text('Códigos de Erro Placa B&R',style: TextStyle(fontWeight: FontWeight.w900,fontSize: 16,color: Colors.white),),
              onTap: () {
                /*Do something*/
                Navigator.pop(context);
                Navigator.push(context, MaterialPageRoute(builder: (context) => ErrorsScreen(isRobo: false,)));
              },
            ),
            ListTile(
              leading: Icon(Icons.android,color: Colors.white),
              title: Text('Códigos de Erro Robô Comau',style: TextStyle(fontWeight: FontWeight.w900,fontSize: 16,color: Colors.white)),
              onTap: () {

                Navigator.pop(context);
                Navigator.push(context, MaterialPageRoute(builder: (context) => ErrorsScreen(isRobo: true,)));

              },
            ),
            ListTile(
              leading: Icon(Icons.message,color: Colors.white),
              title: Text('Chamar Suporte Técnico',style: TextStyle(fontWeight: FontWeight.w900,fontSize: 16,color: Colors.white)),
              onTap: () {

                Navigator.pop(context);
                Navigator.push(context, MaterialPageRoute(builder: (context) => ContactScreen(user: loggedUser,)));
              },
            ),
            ListTile(
              leading: Icon(Icons.camera_alt,color: Colors.white),
              title: Text('Ler QR Code',style: TextStyle(fontWeight: FontWeight.w900,fontSize: 16,color: Colors.white)),
              onTap: () async {
                /*Do something*/
                Navigator.pop(context);
                List<CameraDescription> availableCams;
                availableCams = await availableCameras();
                print(availableCams);
                Navigator.push(context, MaterialPageRoute(builder: (context) => CameraScreen(cameras: availableCams,)));
              },
            ),
            ListTile(
              leading: Icon(Icons.video_library,color: Colors.white),
              title: Text('Videos de Treinamento',style: TextStyle(fontWeight: FontWeight.w900,fontSize: 16,color: Colors.white)),
              onTap: () {
                /*Do something*/
                Navigator.pop(context);
                Navigator.push(context, MaterialPageRoute(builder: (context) => VideoPlayerScreen()));
              },
            ),
            ListTile(
              leading: Icon(Icons.help,color: Colors.white),
              title: Text('Assistente Virtual',style: TextStyle(fontWeight: FontWeight.w900,fontSize: 16,color: Colors.white)),
              onTap: () {
                /*Do something*/
                //Navigator.pop(context);
                Navigator.push(context, MaterialPageRoute(builder: (context) => VideoCallScreen()));
              },
            ),
          ],
        ),
      ),
    );
  }
}
