import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/cupertino.dart';

class Chat {
  final sender;
  final reciever;

  Chat({Key key, this.sender, this.reciever});

  sendMessage(String message) {
    if(message == null){
      return;
    }
    if (int.parse(this.sender["id"]) < int.parse(this.reciever.data["id"])) {
      Firestore.instance
          .collection("messages")
          .document(this.sender["id"] + "_" + this.reciever.data["id"])
          .collection("message")
          .add({
        "text": message,
        "sender": sender["id"],
        "reciever": reciever.data["id"],
        "datetime": DateTime.now().toString(),
        "timestamp": Timestamp.now()
      });
    } else {
      Firestore.instance
          .collection("messages")
          .document(this.reciever.data["id"] + "_" + this.sender["id"])
          .collection("message")
          .add({
        "text": message,
        "sender": sender["id"],
        "reciever": reciever.data["id"],
        "datetime": DateTime.now().toString(),
        "timestamp": Timestamp.now()
      });
    }
  }

  getMessage() {
    if (int.parse(this.sender["id"]) < int.parse(this.reciever.data["id"])) {
      return Firestore.instance.collection("messages").document(this.sender["id"] + "_" + this.reciever.data["id"]).collection("message").orderBy("timestamp").snapshots();
    } else {
      return Firestore.instance.collection("messages").document(this.reciever.data["id"] + "_" + this.sender["id"]).collection("message").orderBy("timestamp").snapshots();
    }
  }
}
