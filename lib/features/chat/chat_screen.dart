import 'dart:convert';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:help_maintenance/features/chat/chat.dart';


class ChatScreen extends StatelessWidget {
  //TODO: USAR O USER BLOC AQUI
  //TODO: USAR O CONTACT BLOC AQUI
  static const id = "Chat_screen";
  final DocumentSnapshot contact;
  final DocumentSnapshot user;
  String message;
  final messageTextController = TextEditingController();


  ChatScreen({Key key,@required this.user , @required this.contact,}) : super(key:key);



  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Color(0xFFf0f0f0),
      appBar: AppBar(
        backgroundColor: Color(0xFF1f345f),
        title: Row(
          children: <Widget>[
           Padding(
             padding: EdgeInsets.fromLTRB(0, 0, 35, 0),
             child:  CircleAvatar(backgroundImage: MemoryImage(base64.decode(contact.data["foto"])),
             radius: 27,),
           ),
            Text(contact.data["nome"]+" "+ contact.data["sobrenome"],style: TextStyle(fontSize: 20),)
          ],
        ),
        centerTitle: true,
      ),
    body:  Column(
    mainAxisAlignment: MainAxisAlignment.spaceBetween,
      crossAxisAlignment: CrossAxisAlignment.stretch,
      children: <Widget>[
        MessageStream(user: user,contact: contact),
        Container(
          decoration: BoxDecoration(
            color: Color(0xFFB4B4B4),
            borderRadius: BorderRadius.only(topLeft: Radius.circular(20),topRight: Radius.circular(20)),
          ),
          child: Row(
            crossAxisAlignment: CrossAxisAlignment.center,
            children: <Widget>[
              Expanded(
                child: TextField(
                  controller: messageTextController,
                  onChanged: (value) {
                   message = value;
                  },
                  decoration:InputDecoration(
                    contentPadding: EdgeInsets.symmetric(vertical: 10.0, horizontal: 20.0),
                    hintText: 'Digite uma mensagem...',
                    border: InputBorder.none,
                  ),
                ),
              ),
              Container(
                margin: EdgeInsets.all(8),
                decoration: BoxDecoration(
                  color: Color(0xFF1f345f),
                  borderRadius: BorderRadius.all(Radius.circular(50.0)),
                ),
                child: IconButton(
                  icon: Icon(Icons.send, color: Colors.white),
                  onPressed: () {
                    messageTextController.clear();
                    Chat(sender: this.user,reciever: this.contact).sendMessage(message);
                  },

                ),
              ),
            ],
          ),
        ),
      ],
    ),
    );
  }
}


class MessageStream extends StatelessWidget {
  final DocumentSnapshot contact;
  final DocumentSnapshot user;

  MessageStream({this.user,this.contact});

  @override
  Widget build(BuildContext context) {
    return StreamBuilder(
      stream: Chat(sender: this.user,reciever: this.contact).getMessage(),
      builder: (context,snapshot){
        if(snapshot.hasData){
          final messages = snapshot.data.documents.reversed.toList();
          List<MessageBubble> messageBubbles = [];
          for (var message in messages){

            final messageText = message.data["text"];
            final messageSender = message.data['sender'];
            final  messageTime = message.data["datetime"];
            String time = messageTime.substring(messageTime.indexOf((new RegExp(r'[0-9]:[0-9]')))-1,messageTime.indexOf(new RegExp(r'[0-9]:[0-9]')) + 4);

            final messageBubble = MessageBubble(sender: messageSender, text: messageText,dateTime: time,isMe: messageSender == this.user["id"],);
            messageBubbles.add(messageBubble);
          }

          return Expanded(
            child: ListView(
              reverse: true,
              padding: EdgeInsets.symmetric(horizontal: 10.0,vertical: 20.0),
              children: messageBubbles,
            ),
          );

        }
        else{

        }
        return Container();
      },
    );
  }
}


class MessageBubble extends StatelessWidget {
  final String sender;
  final String text;
  final String dateTime;
  final bool isMe;

  MessageBubble({this.sender,this.text,this.dateTime,this.isMe});

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: EdgeInsets.all(8.0),
      child: Column(
        crossAxisAlignment: isMe ? CrossAxisAlignment.end : CrossAxisAlignment.start ,
        children: <Widget>[
          Padding(
            padding: EdgeInsets.all(8.0),
            child: Text(dateTime,style: TextStyle(color: Colors.grey),),
          ),
          Material(
            elevation: 5,
            borderRadius:isMe ? BorderRadius.only(topLeft: Radius.circular(20.0),bottomLeft: Radius.circular(20.0),
                bottomRight: Radius.circular(0.0),topRight: Radius.circular(20.0)) : BorderRadius.only(topLeft: Radius.circular(0.0),bottomLeft: Radius.circular(20.0),
                bottomRight: Radius.circular(20.0),topRight: Radius.circular(20.0)),
            color: isMe ?  Color(0xFFb4b4b4):Color(0xFF6a8bc5), //b4b4b4
            child:  Padding(
              padding: EdgeInsets.symmetric(vertical: 10.0, horizontal: 20.0),
              child: Text("$text",style: TextStyle(fontSize: 18,color: Colors.white),),
            ),
          ),
        ],
      ),
    );
  }
}
