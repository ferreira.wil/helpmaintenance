import 'package:flutter/material.dart';


const MessageTextFieldDecoration = InputDecoration(
  contentPadding: EdgeInsets.symmetric(vertical: 10.0, horizontal: 20.0),
  hintText: 'Digite uma mensagem',
  border: InputBorder.none,
);

const MessageContainerDecoration = BoxDecoration(
  color: Colors.grey,
  border: Border(
    top: BorderSide(color: Colors.white, width: 2.0),
  ),
);